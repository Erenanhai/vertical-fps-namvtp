using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    
    public GameMenu gameMenu;
    
    [Range(1, 100)]
    public int enemyCount;

    [Range(2, 30)]
    public float interval;

    

    public static GameManager instance;

    

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
    }
}
