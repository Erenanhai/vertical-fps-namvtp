using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBobbing : MonoBehaviour
{
    [Header("Transform references")]
    public Transform headTransform;
    public Transform cameraTransform;

    [Header("Head bobbing")]
    public float bobFrequency = 5f;
    public float bobHorizontalAmplitude = 0.1f;
    public float bobVerticalAmplitude = 0.1f;
    [Range(0, 1)]
    public float headBobSmoothing = 0.1f;

    [Header("State")]
    public bool isWalking;
    private float walkingTime;
    private Vector3 targetCameraPosition;

    private void Update()
    {
        if (!isWalking)
            walkingTime = 0;
        else
            walkingTime += Time.deltaTime;

        targetCameraPosition = headTransform.position + CalculateHeadBobOffset(walkingTime);

        cameraTransform.position = Vector3.Lerp(cameraTransform.position, targetCameraPosition, headBobSmoothing);

        if ((cameraTransform.position - targetCameraPosition).magnitude <= 0.001)
            cameraTransform.position = targetCameraPosition;
    }

    /// <summary>
    /// Calculate the offset for headbobing camera
    /// </summary>
    /// <param name="walkingTime"></param>
    /// <returns></returns>
    private Vector3 CalculateHeadBobOffset(float walkingTime)
    {
        float horizontalOffset = 0;
        float verticalOffset = 0;
        Vector3 offset = Vector3.zero;

        if(walkingTime > 0)
        {
            horizontalOffset = Mathf.Cos(walkingTime * bobFrequency) * bobHorizontalAmplitude;
            verticalOffset = Mathf.Sin(walkingTime * bobFrequency * 2) * bobVerticalAmplitude;

            offset = headTransform.right * horizontalOffset + headTransform.up * verticalOffset;
        }

        return offset;
    }
}
