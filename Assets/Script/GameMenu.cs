using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Audio;

public class GameMenu : MonoBehaviour
{
    GameManager gameManager;
    AudioManager audioManager;

    [SerializeField] Slider enemyCountControl;
    [SerializeField] Text enemyCountControlDisplay;
    [SerializeField] Slider intervalControl;
    [SerializeField] Text intervalControlDisplay;

    [Header("Pause Screen")]
    public GameObject pauseMenu;
    public static bool isPause = false;

    [Header("Loading Screen")]
    public GameObject loadingScreen;
    public Slider loadingSlider;
    public Text progressText;

    [Header("Win Screen")]
    public GameObject winScreen;

    [Header("Lose Screen")]
    public GameObject loseScreen;

    [HideInInspector]
    public Animator animator;

    public int levelNumber;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        gameManager = FindObjectOfType<GameManager>();
    }

    void Start()
    {
        enemyCountControl.maxValue = 100;
        intervalControl.maxValue = 30f;

        enemyCountControl.value = 10;
        intervalControl.value = 3f;
    }

    private void Update()
    {
        gameManager.enemyCount = Convert.ToInt32(enemyCountControl.value);
        enemyCountControlDisplay.text = gameManager.enemyCount.ToString();

        gameManager.interval = intervalControl.value;
        intervalControlDisplay.text = gameManager.interval.ToString("F2");
    }

    /// <summary>
    /// Load level sceme
    /// </summary>
    /// <param name="level"></param>
    public void LoadLevel(int level)
    {
        StartCoroutine(LoadAsynchronously(level));
    }

    /// <summary>
    /// Load level while displaying loading screen
    /// </summary>
    /// <param name="levelIndex"></param>
    /// <returns></returns>
    IEnumerator LoadAsynchronously (int levelIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(levelIndex);

        loadingScreen.SetActive(true);

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);

            //Debug.Log(progress);
            loadingSlider.value = progress;
            progressText.text = progress * 100 + "%";

            yield return null;
        }
    }

    /// <summary>
    /// Display pause menu
    /// </summary>
    public void Pause()
    {
        pauseMenu.SetActive(true);
        Time.timeScale = 0;
        audioManager.PauseAll();
        isPause = true;
    }

    /// <summary>
    /// Continue the game
    /// </summary>
    public void Resume()
    {
        pauseMenu.SetActive(false);
        Time.timeScale = 1;
        audioManager.Continue();
        isPause = false;
    }

    /// <summary>
    /// Go back to main menu
    /// </summary>
    public void BackToMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("0_Main Menu");
    }

    /// <summary>
    /// Restart level
    /// </summary>
    public void Restart()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    /// <summary>
    /// Display win screen
    /// </summary>
    public void Win()
    {
        Time.timeScale = 0;
        winScreen.SetActive(true);
    }

    /// <summary>
    /// Display lose screen
    /// </summary>
    public void Lose()
    {
        Time.timeScale = 0;
        loseScreen.SetActive(true);
    }

    /// <summary>
    /// Trigger damage screen effect for player
    /// </summary>
    public void Damage()
    {
        animator.SetTrigger("damage");
    }
}
