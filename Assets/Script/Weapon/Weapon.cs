using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    [SerializeField] private BulletPool bulletPool;
    public Transform fpCamera;
    public Transform firePoint;

    [Header("Gun setting")]
    public float bulletSpeed = 10;

    [Header("State")]
    public bool isShooting;
    public float fireSpeed;
    public float fireTimer;

    [Header("Animator")]
    public Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        //bulletPool = BulletPool.main;
    }

    // Update is called once per frame
    void Update()
    {
        if (isShooting)
        {
            if (fireTimer > 0)
                fireTimer -= Time.deltaTime;

            else
            {
                fireTimer = fireSpeed;
                Shoot();
            }
        }
    }

    /// <summary>
    /// Handle shooting
    /// Created by :NamVTP (26/5/2022)
    /// </summary>
    public void Shoot()
    {
        Vector3 bulletVelocity = fpCamera.forward * bulletSpeed;

        bulletPool.PickFromPool(firePoint.position, bulletVelocity);

        FindObjectOfType<AudioManager>().Play("BulletSound");
        //if (simpleGun.aimingEnabled)
            //animator.SetTrigger("aim_fire");
        //else
        animator.SetTrigger("fire");
    }

    /// <summary>
    /// Toggle shooting
    /// Created by :NamVTP (26/5/2022)
    /// </summary>
    public void PullTrigger()
    {
        if (fireSpeed > 0)
            isShooting = true;
        else Shoot();
    }

    /// <summary>
    /// Stop shooting
    /// Created by :NamVTP (26/5/2022)
    /// </summary>
    public void ReleaseTrigger()
    {
        isShooting = false;

        fireTimer = 0;
    }
}
