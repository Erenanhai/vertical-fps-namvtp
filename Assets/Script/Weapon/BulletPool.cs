using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPool : MonoBehaviour
{
    public static BulletPool main;

    [Header("Settings")]
    public GameObject bulletPrefab;
    public int poolSize = 100;

    private List<Bullet> availableBullets;

    private void Awake()
    {
        main = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        availableBullets = new List<Bullet>();

        for (int i = 0; i < poolSize; i++)
        {
            Bullet bullet = Instantiate(bulletPrefab, transform).GetComponent<Bullet>();
            bullet.gameObject.SetActive(false);

            availableBullets.Add(bullet);
        }
    }

    /// <summary>
    /// Spawn bullet
    /// Created by :NamVTP (26/5/2022)
    /// </summary>
    /// <param name="position"></param>
    /// <param name="Velocity"></param>
    public void PickFromPool(Vector3 position, Vector3 Velocity)
    {
        if (availableBullets.Count < 1)
            return;

        availableBullets[0].Activate(position, Velocity);

        availableBullets.RemoveAt(0);
    }

    /// <summary>
    /// Add bullet back to pool
    /// Created by :NamVTP (26/5/2022)
    /// </summary>
    /// <param name="bullet"></param>
    public void AddToPool(Bullet bullet)
    {
        if (!availableBullets.Contains(bullet))
            availableBullets.Add(bullet);
    }
}
