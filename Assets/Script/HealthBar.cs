using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public Slider slider;
    public Gradient gradient;
    public Image fill;

    float healthValue;

    private void Update()
    {
        slider.value = Mathf.Lerp(slider.value, healthValue, 3 * Time.deltaTime);
        fill.color = gradient.Evaluate(slider.normalizedValue);
    }

    /// <summary>
    /// Set HP bar to maximum
    /// </summary>
    /// <param name="health"></param>
    public void SetMaxHealth(float health)
    {
        slider.maxValue = health;
        healthValue = health;

        fill.color = gradient.Evaluate(1);
    }

    /// <summary>
    /// Update HP bar value
    /// </summary>
    /// <param name="health"></param>
    public void SetHealth(float health)
    {
        healthValue = health;
    }
}
