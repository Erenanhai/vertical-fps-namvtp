using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class EnemySpawner : MonoBehaviour
{
    GameManager gameManager;
    public GameMenu menu;

    [SerializeField] private GameObject enemy;
    [SerializeField] private GameObject eliteEnemy;
    //[SerializeField] private GameObject boss;

    [SerializeField] private float interval;

    public int enemyCount;

    public int enemyKilled = 0;
    public int enemyKilledGoal;

    public Text enemyKilledDisplay;
    public Slider progressBar;

    public bool spawnEliteEnemy;
    //public bool spawnBoss;

    public Text enemyRemainingDisplay;
    //public Slider progressBar;

    private void Start() 
    {
        gameManager = FindObjectOfType<GameManager>();
        menu = FindObjectOfType<GameMenu>();

        interval = gameManager.interval;
        enemyCount = gameManager.enemyCount;
        enemyKilledGoal = enemyCount;
        progressBar.maxValue = enemyKilledGoal;

        StartCoroutine(SpawnEnemy(interval, enemy));
    }

    private void Update()
    {
        //enemyRemainingDisplay.text = "Enemies to be spawned: " + enemyCount;
        //progressBar.value = enemyCount;

        enemyKilledDisplay.text = Convert.ToString(enemyKilled);
        progressBar.value = enemyKilled;
        if (enemyKilled > enemyKilledGoal)
        {
            menu.Win();
            enemyKilled = 0;
        }
    }

    /// <summary>
    /// Spawn enemies after sometime
    /// </summary>
    /// <param name="interval"></param>
    /// <param name="enemy"></param>
    /// <returns></returns>
    private IEnumerator SpawnEnemy(float interval, GameObject enemy)
    {
        yield return new WaitForSeconds(interval);
        GameObject newEnemy = Instantiate(enemy, new Vector3(UnityEngine.Random.Range(26, -8), 0, UnityEngine.Random.Range(46, 36)), Quaternion.identity);
        
        enemyCount -= 1;

        if (spawnEliteEnemy || enemyCount % 10 == 0)
        {
                GameObject newEliteEnemy = Instantiate(eliteEnemy, new Vector3(UnityEngine.Random.Range(26, -8), 0, UnityEngine.Random.Range(46, 36)), Quaternion.identity);
        }

        if (enemyCount == 0)
        {
            spawnEliteEnemy = true;
            StopAllCoroutines();
        }
        else
            StartCoroutine(SpawnEnemy(interval, enemy));
    }
}
