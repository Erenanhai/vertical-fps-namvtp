using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    protected Transform target;
    protected NavMeshAgent agent;
    protected Combat combat;

    EnemyStats enemyStats;

    public GameObject[] shootPositions;
    [SerializeField] private float positionChangeCooldown;

    private Vector3 shootPlace;

    // Start is called before the first frame update
    void Start()
    {
        target = PlayerManager.instance.player.transform;
        agent = GetComponent<NavMeshAgent>();
        combat = GetComponent<Combat>();
        enemyStats = GetComponent<EnemyStats>();

        for (int i = 0; i < shootPositions.Length; i++)
            shootPositions[i] = GameObject.Find(Convert.ToString(i));

        StartCoroutine(ChangeDestination());
        //PickRandomDestination();
    }

    // Update is called once per frame
    void Update()
    {
        if (enemyStats.isDead)
            return;
        else
            GoToTarget();
    }

    /// <summary>
    /// Make enemy go toward player
    /// </summary>
    public virtual void GoToTarget()
    {
        agent.SetDestination(shootPlace);

        float distance = Vector3.Distance(shootPlace, transform.position);

        if (distance <= agent.stoppingDistance)
        {
            FaceTarget();
            combat.Attack();
            //StartCoroutine(ChangeDestination());
        }
        if (enemyStats.isDead)
        {
            agent.SetDestination(transform.position);
            return;
        }
    }

    /// <summary>
    /// Choose a random destination in a set of prefixed destinations
    /// </summary>
    private void PickRandomDestination()
    {
        shootPlace = shootPositions[UnityEngine.Random.Range(0, shootPositions.Length - 1)].transform.position;
    }

    /// <summary>
    /// Change destination after 10 seconds
    /// </summary>
    /// <returns></returns>
    private IEnumerator ChangeDestination()
    {
        yield return new WaitForSeconds(positionChangeCooldown);
        PickRandomDestination();
        StartCoroutine(ChangeDestination());
    }



    /// <summary>
    /// Face the player
    /// </summary>
    public virtual void FaceTarget()
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5);
    }

    //private IEnumerator Footsteps()
    //{
    //    FindObjectOfType<AudioManager>().Play("Footstep1");
    //    yield return new WaitForSeconds(0.7f);
    //    FindObjectOfType<AudioManager>().Play("Footstep2");
    //    yield return new WaitForSeconds(0.7f);
    //}
}
