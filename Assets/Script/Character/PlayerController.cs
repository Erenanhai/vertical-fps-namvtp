using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CameraBobbing))]
public class PlayerController : MonoBehaviour
{
    CharacterController characterController;
    public Joystick MovementJoystick;
    public Joystick AimJoystick;
    CameraBobbing cameraBobbing;
    Weapon weapon;

    [Header("First person camera")]
    public Transform fpCameraTransform;

    [Header("Camera Control")]
    Vector2 lookInput;
    float cameraPitch;
    float cameraYaw;

    [Header("Player settings")]
    public float cameraSensitivity;

    [Header("Touch Detection")]
    int upperFingerId;
    int lowerFingerId;
    float moveScreenArea;

    [Header("Player Movement")]
    float horizontalMovement = 0f;
    [SerializeField] float moveSpeed = 10f;

    [Header("Player Action (Fire, Reload)")]
    float aimControlHorizontal = 0f;
    float aimControlVertical = 0f;

    [SerializeField]
    private GameObject fireJoystickHandle;

    [SerializeField]
    [Range(-1, 0)]
    private float triggerDeadzone = -0.3f;

    void Start()
    {
        lowerFingerId = -1;
        upperFingerId = -1;

        moveScreenArea = Screen.height / 3;

        characterController = GetComponent<CharacterController>();
        cameraBobbing = GetComponent<CameraBobbing>();
        weapon = GetComponent<Weapon>();
    }

    
    void Update()
    {
        if (MovementJoystick.Horizontal != 0)
            horizontalMovement = MovementJoystick.Horizontal;
        else
            horizontalMovement = Input.GetAxis("Horizontal");

        //lookInput = AimJoystick.Direction;
        aimControlHorizontal = AimJoystick.Horizontal * cameraSensitivity;
        aimControlVertical = -AimJoystick.Vertical * cameraSensitivity;

        GetTouchInput();

        //if (upperFingerId != -1)
        //    LookAround();

        
        if (horizontalMovement != 0)
        {
            Move();
        }
        else
        {
            cameraBobbing.isWalking = false;
        }

        if (fireJoystickHandle.activeInHierarchy || Input.GetButton("Fire2"))
        {
            LookAround();
            weapon.PullTrigger();
        }
        else
        {
            weapon.ReleaseTrigger();
            fpCameraTransform.localRotation = Quaternion.Slerp(fpCameraTransform.localRotation, Quaternion.identity, Time.deltaTime * 5f);
        }
    }

    /// <summary>
    /// Handle touch inputs
    /// </summary>
    private void GetTouchInput()
    {
        for (int i = 0; i < Input.touchCount; i++)
        {
            Touch touch = Input.GetTouch(i);

            //Debug.Log(horizontalMovement + "," + verticalInput);
            switch (touch.phase)
            {
                //case TouchPhase.Began:
                //    if (touch.position.y > moveScreenArea && upperFingerId == -1)
                //    {
                //        upperFingerId = touch.fingerId;
                //        Debug.Log("Tracking upper finger...");
                //    }
                //    else if (touch.position.y < moveScreenArea && lowerFingerId == -1)
                //    {
                //        lowerFingerId = touch.fingerId;
                //        Debug.Log("Tracking lower finger...");
                //    }
                //    break;

                //case TouchPhase.Ended:
                //    //weapon.ReleaseTrigger();
                //    //break;

                case TouchPhase.Canceled:
                //    if (touch.fingerId == lowerFingerId)
                //    {
                //        lowerFingerId = -1;
                //        Debug.Log("Stop tracking lower finger");
                //        
                //    }
                //    else if (touch.fingerId == upperFingerId)
                //    {
                //        upperFingerId = -1;
                //        Debug.Log("Stop tracking upper finger");
                //    }
                    weapon.ReleaseTrigger();
                    break;

                //case TouchPhase.Moved:
                //    if (touch.fingerId == upperFingerId)
                //        lookInput = touch.deltaPosition * cameraSensitivity * Time.deltaTime;
                //    break;

                //case TouchPhase.Stationary:
                //    if (touch.fingerId == upperFingerId)
                //        lookInput = Vector2.zero;
                //    break;
            }
        }
    }

    /// <summary>
    /// Handle movement
    /// </summary>
    private void Move()
    {
        cameraBobbing.isWalking = true;

        Vector3 move = transform.right * horizontalMovement;
        //transform.translate(horizontalMovement, 0, 0);
        characterController.Move(moveSpeed * Time.deltaTime * move);
    }

    /// <summary>
    /// Handle looking around
    /// </summary>
    private void LookAround()
    {
        //cameraPitch = Mathf.Clamp(cameraPitch - lookInput.y, -90f, 90f);
        //cameraYaw = Mathf.Clamp(cameraYaw + lookInput.x, -90f, 90f);

        cameraPitch = Mathf.Clamp(aimControlVertical, -90f, 90f);
        cameraYaw = Mathf.Clamp(aimControlHorizontal, -90f, 90f);

        fpCameraTransform.localRotation = Quaternion.Euler(cameraPitch, cameraYaw, 0);
    }

    /// <summary>
    /// Handle shooting and other actions
    /// </summary>
    private void Action()
    {
        if (aimControlHorizontal < triggerDeadzone)
            weapon.PullTrigger();
        else
            weapon.ReleaseTrigger();
    }
}
