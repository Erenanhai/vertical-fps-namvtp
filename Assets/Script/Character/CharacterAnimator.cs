using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CharacterAnimator : MonoBehaviour
{
    NavMeshAgent agent;
    protected Animator animator;
    protected Combat combat;
    public AnimatorOverrideController overrideController;
    AudioManager audioManager;

    public AnimationClip[] defaultAttackAnimationSet;
    protected AnimationClip[] currentAttackAnimationSet;
    public AnimationClip replaceableAttackAnimation;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponentInChildren<Animator>();
        combat = GetComponent<Combat>();
        audioManager = FindObjectOfType<AudioManager>();

        if (overrideController == null)
            overrideController = new AnimatorOverrideController(animator.runtimeAnimatorController);
        
        animator.runtimeAnimatorController = overrideController;

        currentAttackAnimationSet = defaultAttackAnimationSet;
        combat.OnAttack += OnAttack;
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        float speedPercent = agent.velocity.magnitude / agent.speed;
        animator.SetFloat("Vertical", speedPercent, .1f, Time.deltaTime);

        animator.SetBool("inCombat", combat.InCombat);
    }

    /// <summary>
    /// Handle attack animations
    /// </summary>
    protected virtual void OnAttack()
    {
        animator.SetTrigger("attack");

        int attackIndex = Random.Range(0, currentAttackAnimationSet.Length);
        overrideController[replaceableAttackAnimation.name] = currentAttackAnimationSet[attackIndex];
    }

    public void PlayStepSound(int index)
    {
        if (index == 1)
            audioManager.Play("Footstep1");
        else if (index == 2)
            audioManager.Play("Footstep2");
    }
}
