using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStats : CharacterStats
{
    Animator animator;
    public EnemySpawner enemySpawner;
    public bool isDead;
    //[SerializeField] private GameMenu menu;

    private void Awake()
    {
        animator = GetComponentInChildren<Animator>();
        enemySpawner = FindObjectOfType<EnemySpawner>();
        isDead = false;
        //menu = FindObjectOfType<GameMenu>();
    }

    private void Start()
    {
        currentHealth = maxHealth;
    }

    /// <summary>
    /// Handle damage
    /// </summary>
    /// <param name="damage"></param>
    public override void TakeDamage(int damage)
    {
        if (isDead)
            return;
        else
        {
            base.TakeDamage(damage);

            currentHealth -= damage;

            if (currentHealth <= 0)
            {
                currentHealth = 0;
                Die();
            }
        }
        
    }

    /// <summary>
    /// Handle dead
    /// </summary>
    public override void Die()
    {
        isDead = true;
        enemySpawner.enemyKilled += 1;
        animator.Play("Die");
        Destroy(gameObject, 0.8f);
        //if (gameObject.name == "Elite Enemy(Clone)")
        //    menu.Win();
        return;
    }
}
