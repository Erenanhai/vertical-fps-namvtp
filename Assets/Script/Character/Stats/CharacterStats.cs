using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CharacterStats : MonoBehaviour
{
    [Header("Health")]
    public float maxHealth;
    public float currentHealth;

    public HealthBar healthBar;

    /// <summary>
    /// Handle damage
    /// </summary>
    /// <param name="damage"></param>
    public virtual void TakeDamage(int damage)
    {
        Debug.Log(transform.name + " took " + damage + " damages.");
    }

    /// <summary>
    /// Handle dead
    /// </summary>
    public abstract void Die();
}
