using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : CharacterStats
{
    public GameMenu gameMenu;
    private void Start()
    {
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Keypad5))
        {
            TestDamage();
        }
    }

    /// <summary>
    /// For testing damage only
    /// </summary>
    void TestDamage()
    {
            TakeDamage(20);
    }

    /// <summary>
    /// Handle damage
    /// </summary>
    /// <param name="damage"></param>
    public override void TakeDamage(int damage)
    {
        base.TakeDamage(damage);

        gameMenu.Damage();

        currentHealth -= damage;

        if(currentHealth <= 0)
        {
            currentHealth = 0;
            Die();
        }

        healthBar.SetHealth(currentHealth);
    }

    /// <summary>
    /// Handle dead and lose
    /// </summary>
    public override void Die()
    {
        //RIP
        gameMenu.Lose();
    }
}
