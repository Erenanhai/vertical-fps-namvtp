using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Combat : MonoBehaviour
{
    public float attackSpeed = 1;
    private float attackCooldown = 0;
    const float combatCooldown = 5;
    float lastAttackTime;

    public bool InCombat { get; private set; }
    public event System.Action OnAttack;
    

    private void Update()
    {
        attackCooldown -= Time.deltaTime;

        if(Time.time - lastAttackTime > combatCooldown)
        {
            InCombat = false;
        }
    }

    /// <summary>
    /// Handle enemy attack
    /// </summary>
    public void Attack()
    {
        if (attackCooldown <= 0) 
        {
            //Attack
            Debug.Log("Enemy attack!");

            StartCoroutine(DoAttack());

            if (OnAttack != null)
                OnAttack();

            attackCooldown = 1f / attackSpeed;
            InCombat = true;
            lastAttackTime = Time.time;
        }
    }

    /// <summary>
    /// Delay for each attack
    /// </summary>
    /// <returns></returns>
    IEnumerator DoAttack()
    {
        yield return new WaitForSeconds(.8f);
        // Attack
    }
}
